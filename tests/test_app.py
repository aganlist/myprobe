import pytest


@pytest.mark.parametrize("route", ["/clients", "/clients/1"])
def test_route_status(client, route):
    rv = client.get(route)
    assert rv.status_code == 200


def test_create_client(client) -> None:
    client_data = {"name": "Никита", "surname": "Нестеренко",
                   "credit_card": "1212121212", "car_number": "A432BB55"}
    resp = client.post("/clients", data=client_data)

    assert resp.status_code == 201


def test_create_parking(client) -> None:
    parking_data = {"address": "L.Tolstogo, 105", "opened": True,
                    "count": 20, "available": 18}
    resp = client.post("/parkings", data=parking_data)

    assert resp.status_code == 201


@pytest.mark.parking
def test_place_close(client) -> None:  # тест заезда на парковку
    place_data = {"client_id": 1, "parking_id": 1}
    resp = client.post("/client_parkings", data=place_data)

    assert resp.status_code == 201


@pytest.mark.parking
def test_place_unclose(client) -> None:  # тест выезда с парковки
    place_data = {"client_id": 2, "parking_id": 1}
    resp = client.delete("/client_parkings", data=place_data)

    assert resp.status_code == 200
