import pytest
import datetime

from src.app import create_app, db as _db
from src.models import Client, Parking, Client_parking


@pytest.fixture
def app():
    _app = create_app()
    _app.config["TESTING"] = True
    _app.config["SQLALCHEMY_DATABASE_URI"] = "sqlite://"

    with _app.app_context():
        _db.create_all()
        client_1 = Client(id=1,
                          name="Иван",
                          surname="Иванов",
                          credit_card='1234567890',
                          car_number='A123BC45')
        parking = Parking(id=1,
                          address='Pushkina, 34',
                          opened=True,
                          count_places=10,
                          count_available_places=4)
        client_2 = Client(id=2,
                          name="Петр",
                          surname="Петров",
                          credit_card='2345678901',
                          car_number='A678BC490')

        client_parking = Client_parking(id=1,
                                        client_id=2,
                                        parking_id=1,
                                        time_in=datetime.datetime.now())
        _db.session.add(client_1)
        _db.session.add(client_2)
        _db.session.add(parking)
        _db.session.add(client_parking)
        _db.session.commit()

        yield _app
        _db.session.close()
        _db.drop_all()


@pytest.fixture
def client(app):
    client = app.test_client()
    yield client


@pytest.fixture
def db(app):
    with app.app_context():
        yield _db
