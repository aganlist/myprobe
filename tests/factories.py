import factory, string
import factory.fuzzy as fuzzy
import random

import faker
from src.app import db
from src.models import Client, Parking, Client_parking

num_list = ['1', '2', '3', '4', '5', '6', '7', '8', '9', '0']


class ClientFactory(factory.alchemy.SQLAlchemyModelFactory):
    class Meta:
        model = Client
        sqlalchemy_session = db.session

    name = factory.Faker('first_name')
    surname = factory.Faker('last_name')
    credit_card = factory.fuzzy.FuzzyText(length=10, chars=num_list) if random.random() < 0.7 else None
    car_number = factory.fuzzy.FuzzyText(length=8, chars=string.ascii_letters)


class ParkingFactory(factory.alchemy.SQLAlchemyModelFactory):
    class Meta:
        model = Parking
        sqlalchemy_session = db.session

    address = factory.Faker('address')
    opened = factory.fuzzy.FuzzyChoice([True, False])
    count_places = factory.fuzzy.FuzzyInteger(22, 50)
    count_available_places = factory.LazyAttribute(lambda x: random.randrange(1, 20))
