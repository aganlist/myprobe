import datetime
from flask_sqlalchemy import SQLAlchemy
from flask import Flask, request, jsonify


db = SQLAlchemy()


def create_app():
    app = Flask(__name__)
    app.config["SQLALCHEMY_DATABASE_URI"] = "sqlite:///parking.db"
    db.init_app(app)

    from src.models import Client, Parking, Client_parking

    @app.before_first_request
    def before_request_func():
        db.create_all()

    @app.teardown_appcontext
    def shutdown_session(exception=None):
        db.session.remove()

    @app.route("/clients", methods=["POST"])
    def create_client_handler():
        """Создание нового клиента"""
        name = request.form.get("name", type=str)
        surname = request.form.get("surname", type=str)
        credit_card = request.form.get("credit_card", type=str)
        car_number = request.form.get("car_number", type=str)

        new_client = Client(
            name=name, surname=surname, credit_card=credit_card, car_number=car_number
        )

        db.session.add(new_client)
        db.session.commit()

        return new_client.to_json(), 201

    @app.route("/clients", methods=["GET"])
    def get_clients_handler():
        """Получение клиентов"""
        clients = db.session.query(Client).all()
        clients_list = [c.to_json() for c in clients]
        return jsonify(clients_list), 200

    @app.route("/clients/<client_id>", methods=["GET"])
    def get_client_handler(client_id: int):
        """Получение клиента по ид"""
        client = db.session.query(Client).get(client_id)
        return jsonify(client.to_json()), 200

    @app.route("/parkings", methods=["POST"])
    def create_parking_handler():
        """Создание новой парковочной зоны"""
        address = request.form.get("address", type=str)
        opened = request.form.get("opened", type=bool)
        count_places = request.form.get("count", type=int)
        count_available_places = request.form.get("available", type=int)

        new_parking = Parking(
            address=address,
            opened=opened,
            count_places=count_places,
            count_available_places=count_available_places,
        )

        db.session.add(new_parking)
        db.session.commit()
        return new_parking.to_json(), 201

    @app.route("/client_parkings", methods=["POST"])
    def add_client_parkings_handler():
        """Заезд на парковку"""
        client_id = request.form.get("client_id", type=int)
        parking_id = request.form.get("parking_id", type=int)

        place = (
            db.session.query(Client_parking)
                .filter(
                Client_parking.parking_id == parking_id,
                Client_parking.client_id == client_id,
                Client_parking.time_out == None,
            )
                .one_or_none()
        )
        if place:  # может клиент уже заехал ранее?
            return "{'error': 'action not feasible'}", 404

        parking = db.session.query(Parking).get(parking_id)
        if parking is not None:
            if parking.opened and parking.count_available_places:
                parking.count_available_places -= 1
                time_in = datetime.datetime.now()

                new_place = Client_parking(
                    client_id=client_id, parking_id=parking_id, time_in=time_in
                )
                db.session.add(new_place)
                db.session.commit()
                return new_place.to_json(), 201
            else:
                return "{'error': 'no place'}", 404
        else:
            return "{'error': 'no such parking'}", 404

    @app.route("/client_parkings", methods=["DELETE"])
    def del_client_parkings_handler():
        """Выезд с парковки"""
        client_id = request.form.get("client_id", type=int)
        parking_id = request.form.get("parking_id", type=int)

        place = (
            db.session.query(Client_parking)
                .filter(
                Client_parking.parking_id == parking_id,
                Client_parking.client_id == client_id,
                Client_parking.time_out == None,
            )
                .one_or_none()
        )
        parking = db.session.query(Parking).get(parking_id)
        card = (
            db.session.query(Client.credit_card)
                .filter(Client.id == client_id)
                .one_or_none()
        )

        if place and parking.opened and card[0]:
            time_out = datetime.datetime.now()
            if time_out >= place.time_in:
                parking.count_available_places += 1
                place.time_out = datetime.datetime.now()
                db.session.commit()
                return place.to_json(), 200
            else:
                return "{'error': 'time_out < time_in'}", 404
        else:
            return "{'error': 'action not feasible'}", 404

    return app


if __name__ == "__main__":
    app = create_app()
    app.run()
